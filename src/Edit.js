import React, { Component } from "react";

function handleErrors(response) {
  if (!response.ok) {
      throw Error(response.statusText);
  }
  console.log(response);
  window.location.href = "/";
}

class Edit extends Component {

  constructor() {
    super();
    this.state = {
        nama: '',
        namaold:  '',
        nomor_mr: ''
    }
        this.handleFieldChange = this.handleFieldChange.bind(this)
        
  }


  handleFieldChange (event) {
    this.setState({
        [event.target.name]: event.target.value
    })
}
  componentDidMount() {
    const { userName } = this.props.match.params
      fetch("http://localhost:3333/users/"+userName)
        .then(res => res.json())
        .then(parsedJSON => parsedJSON.map(data => (          
            this.setState({
              nama: data.nama,
              namaold:  data.nama,
              nomor_mr: data.nomor_mr 
            })           
          
        )))       
        .catch(error => console.log('parsing failed', error))
    }

    onSubmit = (e) => {
        e.preventDefault();
        
        const {  namaold } = this.state;
        // console.log(this.state)
    
        fetch('http://localhost:3333/users/'+namaold, {
          method: 'put',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(this.state)
        })
        .then(handleErrors)
        .then(response => response.json() )
        .catch(error => console.log(error) );
        
      }

    
    render() {
      const { nama,nomor_mr } = this.state
      var nama2 = nama.replace(/\s/g, "");
      var nomor_mr2 = nomor_mr.replace(/\s/g, "");

        return (
          <div className="boxWhite">
            <h2>List User</h2> 
                        <form onSubmit={this.onSubmit}>
                            <input type="hidden" class="form-control" id="namaold" name="namaold"  value={this.state.namaold} />
                       
                        <div class="form-group">
                            <label for="nama">Nama:</label>
                            <input type="text" class="form-control" id="nama" name="nama"  onChange={this.handleFieldChange} defaultValue ={nama2} />
                        </div>
                        <div class="form-group">
                            <label for="nomor_mr">No MR:</label>
                            <input type="text" class="form-control" id="nomor_mr" name="nomor_mr"  onChange={this.handleFieldChange} defaultValue={nomor_mr2}/>
                        </div>
                        
                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>                        
          </div>
        );
    }
  }

export default Edit;