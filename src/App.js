// import logo from './logo.svg';
import './App.css';
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Home from "./Home";
import Add from "./Add";
import Edit from "./Edit";

function App() {  
  return (
    <HashRouter>
    <div className="App">
        <div className="headeratas">
           <NavLink className="menu" exact to="/">Home</NavLink>
           <NavLink className="menu" exact to="/Add">Add</NavLink>
        </div>
        <div>      
            <div className="content">
              <Route exact path="/" component={Home}/>
              <Route exact path="/Add" component={Add}/>
              <Route exact path="/Edit/:userName" component={Edit}/>
            </div>
        </div>
    </div>
    </HashRouter>
  
  );
}

export default App;
