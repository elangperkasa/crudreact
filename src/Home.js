import React, { Component } from "react";
import {Link} from 'react-router-dom'

function handleErrors(response) {
  if (!response.ok) {
      throw Error(response.statusText);
  }
  // console.log(response);
  window.location.href = "/";
}

class Home extends Component {

  constructor() {
    super();
    this.state = {
      items: []
    };
  }

  deleteHandler(nama, e) {
    e.preventDefault();
    console.log("namanya="+nama);
     fetch("http://localhost:3333/users/"+nama
     , {
      method: 'delete',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }
     )
     .then(handleErrors)
     .then(console.log("deleted"))
     .catch(error => console.log(error)
     
     );
}


  componentDidMount() {
      fetch("http://localhost:3333/users")
        .then(res => res.json())
        .then(parsedJSON => parsedJSON.map(data => (
          {
            Nama: data.nama,
            Nomr: data.nomor_mr          
          }
        )))
        .then(items => this.setState({
          items,
          isLoaded: false
        }))
        .catch(error => console.log('parsing failed', error))
    }

    render() {
      const {items } = this.state;
        return (
          <div className="boxWhite">
            <h2>List User</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">No MR</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        
                    items.length > 0 ? items.map(item => {
                    const {Nama,Nomr} = item;
                    return (

                        <tr>
                          <td>{Nama}</td>
                          <td>{Nomr} </td>                            
                          <td>
                                              
                            <Link to={"/edit/"+Nama} className="btn btn-primary">Edit</Link>&nbsp;&nbsp;&nbsp;
                            <button onClick={this.deleteHandler.bind(this, Nama)}className="btn btn-danger">Delete</button>
                          
                          </td>                          
                      </tr>  
                    );
                    }) : null
                }
                </tbody>
            </table>                  
          </div>

        );

    }
  }

export default Home;