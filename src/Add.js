import React, { Component } from "react";
import {
    Container
  } from 'reactstrap';

function handleErrors(response) {
  if (!response.ok) {
      throw Error(response.statusText);
  }
  // return response;
  window.location.href = "/";
}

class Add extends Component {
  constructor() {
    super();
    this.state = {
      nama: '',
      nomor_mr: ''
    };
  }

  onChange = (e) => {
    this.setState({[e.target.name]: e.target.value });   
  }

  
  onSubmit = (e) => {
    e.preventDefault();
    
    console.log(this.state)

    fetch('http://localhost:3333/users', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(this.state)
    })
    .then(handleErrors)
    .then(response => response.json() )   
    .catch(error => console.log(error) );    
    
  }

  render() {
        return (            
          <div className="boxWhite">
                <h2>Add User</h2>
                <Container className="App">
                    <form onSubmit={this.onSubmit}>
                        <div class="form-group">
                            <label for="nama">Nama:</label>
                            <input type="text" class="form-control" id="nama" name="nama" onChange={this.onChange} />
                        </div>
                        <div class="form-group">
                            <label for="nomor_mr">No MR:</label>
                            <input type="text" class="form-control" id="nomor_mr" name="nomor_mr" onChange={this.onChange}/>
                        </div>
                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                </Container>
          </div>

        );

    }
  }

export default Add;